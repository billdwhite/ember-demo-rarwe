import Ember from 'ember';

export default Ember.Controller.extend({
    isEditing: false,
    actions: {
        edit: function() {
            this.set('isEditing', true);
        },
        save: function() { // change in controller/view state handled here
            this.set('isEditing', false);
            return true; // allow multiple handling; doing this means action is sent up to the current route
        }
    }
});
