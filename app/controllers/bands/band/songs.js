import Ember from 'ember';
import { capitalizeWords } from '../../../helpers/capitalize-words';

export default Ember.Controller.extend({
    needs: ['bands/band'],
    queryParams: {
        sortBy: 'sort',
        searchTerm: 's'
    },
    title: '',
    newSongPlaceholder: Ember.computed('controllers.bands/band.model.name', function() {
        var band = this.get('controllers.bands/band.model');
        return "New %@ song".fmt(capitalizeWords(band.get('name')));
    }),
    songCreationStarted: false,
    isAddButtonDisabled: Ember.computed('title', function() {
        return Ember.isEmpty(this.get('title'));
    }),
    canCreateSong: Ember.computed('songCreationStarted', 'model.length', function() {
        return this.get('songCreationStarted') || this.get('model.length');
    }),
    sortBy: 'ratingDesc',
    searchTerm: '',
    sortProperties: Ember.computed('sortBy', function() {
        var options = {
            "ratingDesc": "rating:desc,title:asc",
            "ratingAsc":  "rating:asc,title:asc",
            "titleDesc":  "title:desc",
            "titleAsc":   "title:asc"
        };
        return options[this.get('sortBy')].split(',');
    }),
    sortedSongs: Ember.computed.sort('matchingSongs', 'sortProperties'),
    matchingSongs: Ember.computed('model.@each.title', 'searchTerm', function() {
        var searchTerm = this.get('searchTerm').toLowerCase();
        return this.get('model').filter(function(song) {
            return song.get('title').toLowerCase().indexOf(searchTerm) !== -1;
        });
    }),
    actions: {
        enableSongCreation: function() {
            this.set('songCreationStarted', true);
        },
        setSorting: function(option) {
            this.set('sortBy', option);
        }
    }
});
