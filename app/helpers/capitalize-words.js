import Ember from 'ember';

export function capitalizeWords(input/*, hash*/) {
    var words = input.toString().split(/\s+/).map(function(word) {
        return word.toLowerCase().capitalize();
    });
    console.log('capitalizeWords(' + input + ') = ' + words);
    return words.join(' ');
}

export default Ember.Helper.helper(capitalizeWords);    // takes ordinary function and wraps it so that the helper function establishes a binding; if the prop
                                                        // passed to the helper changes, the helper re-renders
