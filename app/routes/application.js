import Ember from 'ember';

export default Ember.Route.extend({
    beforeModel: function(transition) { // called before resolved model is known; param is the current transition
        this._super(transition);
        console.log('beforeModel()', transition);
    },
    afterModel: function(model, transition) { // called before resolved model is known; param is the current transition
        this._super(model, transition);
        console.log('afterModel()', transition);
    },
    setupController: function(controller, model, transition) {
        this._super(controller, model, transition);
        console.log('setupController()', [controller, model, transition]);
    },
    actions: {
        willTransition: function(transition) { // triggered on source route before the transition starts ->
            console.log('willTransition', transition);
        },
        didTransition: function() { // -> triggered on destination route once transition has finished
            console.log('didTransition');
        }
    }
});
