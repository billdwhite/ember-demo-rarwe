import Ember from 'ember';

// simulate network latency
function wait(promise, delay) {
    return new Ember.RSVP.Promise(function(resolve) {
        setTimeout(function() {
            promise.then(function(result) {
                resolve(result);
            });
        }, delay);
    });
}
export default Ember.Route.extend({
    model: function() {
        var bands = this.store.findAll('band');
        return wait(bands, 1 * 1000);
    },
    actions: { // actions hash
        willTransition: function(transition) {
            console.log('willTransition', transition);
        },
        didTransition: function() {
            console.log('didTransition');
            Ember.$(document).attr('title', 'Bands - Rock & Roll');
        },
        createBand: function() {
            var route = this;
            var controller = this.get('controller');
            var band = this.store.createRecord('band', controller.getProperties('name')); // create band in store and save name
            band.save().then(function() { // promise function
                controller.set('name', ''); // clear name field now that we are done
                route.transitionTo('bands.band.songs', band); // show songs for new band
            });
        }
    }
});
