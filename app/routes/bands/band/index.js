import Ember from 'ember';

export default Ember.Route.extend({
    afterModel: function(band) { // called before resolved model is known; param is the current transition
        var description = band.get('description');
        if (Ember.isEmpty(description)) {
            this.transitionTo('bands.band.songs');
        } else {
            this.transitionTo('bands.band.details');
        }
    }
});
