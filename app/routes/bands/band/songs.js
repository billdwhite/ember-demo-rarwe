import Ember from 'ember';
import { capitalizeWords as capitalize } from '../../../helpers/capitalize-words';

// simulate network latency
function wait(promise, delay) {
    return new Ember.RSVP.Promise(function(resolve) {
        setTimeout(function() {
            promise.then(function(result) {
                resolve(result);
            });
        }, delay);
    });
}
export default Ember.Route.extend({
    model: function() {
        var songs = Ember.RSVP.resolve(this.modelFor('bands.band').get('songs')); //retrieve the band that was returned one level higher and get its songs
        return wait(songs, 1000);
    },
    actions: {
        createSong: function() {
            var controller = this.controller;
            var band = this.modelFor('bands.band');
            var song = this.store.createRecord('song', {
                title: controller.get('title'),
                band: band
            });
            song.save().then(function() {
                controller.set('title', '');
            });
        },
        updateRating: function(params) {
            console.log('updateRating');
            var song = params.item;
            var rating = params.rating;
            if (song.get('rating') === rating) {
                rating = 0;
            }
            song.set('rating', rating);
            song.save();
        },
        didTransition: function() {
            var band = this.modelFor('bands.band');
            var name = capitalize(band.get('name'));
            Ember.$(document).attr('title', '%@ songs - Rock & Roll'.fmt(name));
        }
    }
});
