/* jshint node: true */
var contentSecurityPolicy = {
    'default-src': "'none'",
        'script-src': "'self' 'unsafe-inline' 'unsafe-eval' use.typekit.net connect.facebook.net maps.googleapis.com maps.gstatic.com api.github.com",
        'font-src': "'self' data: use.typekit.net",
        'connect-src': "'self' api.github.com localhost:* api.rockandrollwithemberjs.com:*",
        'img-src': "'self' www.facebook.com p.typekit.net",
        'style-src': "'self' 'unsafe-inline' use.typekit.net",
        'frame-src': "s-static.ak.facebook.com static.ak.facebook.com www.facebook.com"
};

module.exports = function(environment) {
    var ENV = {
        modulePrefix: 'rarwe',
        environment: environment,
        baseURL: '/',
        locationType: 'auto',
        EmberENV: {
            FEATURES: {
                // Here you can enable experimental features on an ember canary build
                // e.g. 'with-controller': true
            }
        },
        contentSecurityPolicy: contentSecurityPolicy,
        APP: {
            // Here you can pass flags/options to your application instance
            // when it is created
        }
    };

    if (environment === 'development') {
        // ENV.APP.LOG_RESOLVER = true;
        ENV.APP.LOG_ACTIVE_GENERATION = true;
        // ENV.APP.LOG_TRANSITIONS = true;
        // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
        ENV.APP.LOG_VIEW_LOOKUPS = true;
        ENV.contentSecurityPolicy = contentSecurityPolicy;
        ENV.apiHost = 'http://api.rockandrollwithemberjs.com/';
    }

    if (environment === 'test') {
        // Testem prefers this...
        ENV.baseURL = '/';
        ENV.locationType = 'none';

        // keep test console output quieter
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;

        ENV.APP.rootElement = '#ember-testing';
        ENV.contentSecurityPolicy = contentSecurityPolicy;
        ENV.apiHost = '';
    }

    if (environment === 'production') {

    }

    return ENV;
};
