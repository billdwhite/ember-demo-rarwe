import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';


moduleForComponent('star-rating', 'Unit | Component | star rating', {
    unit: true
});

test('it renders', function(assert) {
    assert.expect(4);

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });
    this.render(hbs`{{star-rating}}`);

    // Template block usage:
    this.render(hbs`
        {{#star-rating}}
          template block text
        {{/star-rating}}
      `);

    var component = this.subject(); // this.subject() is the unit under test (the star-rating component)
    Ember.run(function() {
        component.setProperties({
            rating: 4,
            maxRating: 5
        });
    });

    assert.equal(this.$().find('.glyphicon-star').length, 4, "The right amount of full stars is rendered");
    assert.equal(this.$().find('.glyphicon-star-empty').length, 1, "The right amount of empty stars is rendered");

    Ember.run(function() {
        component.set('maxRating', 10);
    });

    assert.equal(this.$().find('.glyphicon-star').length, 4, "The right amount of full stars is rendered");
    assert.equal(this.$().find('.glyphicon-star-empty').length, 6, "The right amount of empty stars is rendered after changing maxRating");
});
